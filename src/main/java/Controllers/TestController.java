package Controllers;

import Dao.DaoImpl1;
import Dao.Idao;
import Services.Mitierimpl1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/testController")
public class TestController {

    private Mitierimpl1 imitier;

    public Mitierimpl1 getMitierimpl1() {
        return imitier;
    }
    @Autowired
    public void setMitierimpl1(Mitierimpl1 imitier) {
        this.imitier = imitier;
    }

    @ResponseBody
    @GetMapping("/testindex")
    public  String index(){
        return "salut ceci est un test";
    }

    @GetMapping("/hellopage")
    public String hellopage(){
        return  "hellojsp";
    }
    @ResponseBody
    @GetMapping("/serviceprint")
    public  String afficheservice(){
        return imitier.printtest();
    }

    @ResponseBody
    @GetMapping ("/servicedata")
    public String afficherdata (){
        return imitier.getdatasevice();
    }
}
