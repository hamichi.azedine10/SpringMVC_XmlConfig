package Services;

import Dao.DaoImpl1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("Mitierimplimentation")
public class Mitierimpl1  // implements Imitier
{

    DaoImpl1 idao ;

    public DaoImpl1 getIdao() {
        return idao;
    }

    @Autowired
    public void setIdao(DaoImpl1 idao) {
        this.idao = idao;
    }

   // @Override
    public  String  printtest (){
        System.out.println("ceci est un affichage de la couche service ");
        return "ceci est un affichage de la couche service ";
    }

  //  @Override
    public String getdatasevice() {
      return idao.getdata();
    }
}
